import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { media } from '../../assets/media'
import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'
import CustomIcon from '../CustomIcon'
import { screenWidth } from '../../utils/constants'
import { banner_data } from '../../global/sampleData'


const HomeBanner = ({}) => {

    const [data, setData] = useState(banner_data);

    return (
        <View style={styles.container} >
            <FlatList
                data={data}
                keyExtractor={item => item.id}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({item, index}) => (
                    <View key={index} style={[styles.banner, {backgroundColor: item?.bg_color}]} >
                        <View style={{marginBottom: 20, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between', }}>
                            <View>
                                <AppText
                                    text={"Total Wallet Balance"}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                    customStyle={{color: colors.white  }}
                                />
                                <AppText
                                    text={`$${item?.wallet_balance}`}
                                    type={STRING_CONSTANTS.textConstants.HEADING}
                                    customStyle={{color: colors.white, marginTop: 4, fontWeight: 'bold' }}
                                />
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                <AppText
                                    text={"USD"}
                                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                                    customStyle={{color: colors.white, marginRight: 4, fontWeight: 'bold', }}
                                />
                                <CustomIcon
                                    iconName="chevron-down"
                                    iconType="Feather"
                                    iconSize={24}
                                    iconColor={colors.white}
                                />
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', }}>
                            <View>
                                <AppText
                                    text={"Weekly Profit"}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                    customStyle={{color: colors.white  }}
                                />
                                <AppText
                                    text={`$${item?.weekly_profit}`}
                                    type={STRING_CONSTANTS.textConstants.HEADING}
                                    customStyle={{color: colors.white, marginTop: 4, fontWeight: 'bold'  }}
                                />
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                <CustomIcon
                                    iconName="caretdown"
                                    iconType="AntDesign"
                                    iconSize={16}
                                    iconColor={colors.white}
                                />
                                <AppText
                                    text={`+${item?.percentage}%`}
                                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                                    customStyle={{color: colors.white, marginLeft: 4, fontWeight: 'bold' }}
                                />
                                
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 10,
    },
    banner: {
        padding: 20,
        borderRadius: 15,
        marginRight: 20,
        width: screenWidth - 80,
    },
})

export default HomeBanner