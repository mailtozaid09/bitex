import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'

import { STRING_CONSTANTS, colors } from '../../../utils'
import AppText from '../../text'


const ForgotPassword = ({ onPress}) => {
    return (
        <View style={styles.container} >
            
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={"Forgot Password"}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: '#FDAE67', textDecorationLine: 'underline', }}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'flex-end'
    }
})

export default ForgotPassword