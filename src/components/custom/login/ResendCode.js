import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'

import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import AppText from '../../text'


const ResendCode = ({ title, onPress}) => {
    return (
        <View style={styles.container} >
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.CAPTION1}
                customStyle={{ fontSize: 16, lineHeight: 24, marginRight: 10, fontFamily: fonts.primary_regular_font, fontWeight: '400', color: colors.white, textAlign: 'center', }}
            />
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={"Resend Code"}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: '#FDAE67', fontSize: 16, lineHeight: 24, textDecorationLine: 'underline', fontFamily: fonts.primary_regular_font, fontWeight: '500',  }}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
    }
})

export default ResendCode