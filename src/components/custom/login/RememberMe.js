import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, Platform } from 'react-native'

import { STRING_CONSTANTS, colors } from '../../../utils'
import AppText from '../../text'
import CheckBox from '@react-native-community/checkbox'


const RememberMe = ({toggleCheckBox, onToggle, onPress}) => {
    return (
        <View style={styles.container} >
            <View style={styles.checkboxContainer}>
                <CheckBox
                    lineWidth={1}
                    tintColors={{ true: colors.sea_green, false: colors.light_gray }} 
                    onCheckColor="#fff" 
                    onTintColor={colors.sea_green}
                    onFillColor={colors.sea_green}
                    tintColor={colors.light_gray}
                    boxType="square"
                    value={toggleCheckBox}
                    onValueChange={(newValue) => onToggle(newValue)}
                    style={styles.checkbox}
                />
            </View>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={"Remember me"}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: colors.white, marginLeft: 10, marginBottom: 6 }}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    checkboxContainer: {
        width: Platform.OS == 'android' ? 15 : 10,
        height: Platform.OS == 'android' ? 15 : 10, 
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkbox: {
        transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }], 
    },
})

export default RememberMe