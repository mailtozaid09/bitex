import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, } from 'react-native'

import { STRING_CONSTANTS, colors } from '../../../utils'
import AppText from '../../text'


const AlreadyHaveAccount = ({title, buttonText, buttonColor, signColor, onPress}) => {
    return (
        <View style={styles.container} >
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.BODY}
                customStyle={{textAlign: 'center', color: buttonColor ? buttonColor : colors.white}}
            />
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={buttonText}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: signColor ? signColor : colors.primary, textDecorationLine: 'underline', marginLeft: 4}}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default AlreadyHaveAccount