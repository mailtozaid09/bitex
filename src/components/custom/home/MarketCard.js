import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { media } from '../../../assets/media'
import AppText from '../../text'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import CustomIcon from '../../CustomIcon'
import { screenWidth } from '../../../utils/constants'
import { banner_data, portfolio_data } from '../../../global/sampleData'
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'
import { useNavigation } from '@react-navigation/native'


const MarketCard = ({onPress}) => {

    const navigation = useNavigation()

    const [data, setData] = useState(portfolio_data);

    return (
        <View style={styles.container} >

            <View style={{marginVertical: 10, marginBottom: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                <AppText
                    text={"Market"}
                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                    customStyle={{color: colors.white, fontSize: 20, fontFamily: fonts.primary_medium_font, fontWeight: '600'  }}
                />
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onPress}
                >
                    <AppText
                        text={"View All+"}
                        type={STRING_CONSTANTS.textConstants.HEADLINE3}
                        customStyle={{color: '#363D4E', fontSize: 15, fontFamily: fonts.primary_medium_font, fontWeight: '600' }}
                    />
                </TouchableOpacity>
            </View>

            <FlatList
                data={data}
                keyExtractor={item => item.id}
                showsHorizontalScrollIndicator={false}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        onPress={() => {navigation.navigate(NAVIGATIONS.PORTFOLIO_DETAILS, {params: item})}}
                        key={index} style={[styles.banner, {backgroundColor: colors.bg_color}]} >

                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                <Image source={item?.icon} style={{height: 50, width: 50, marginRight: 15, resizeMode: 'contain'}} />
                                <View>
                                    <AppText
                                        text={item?.title}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                        customStyle={{color: colors.sea_green, }}
                                    />
                                    <AppText
                                        text={item?.subTitle}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                        customStyle={{color: '#A6A3B8', marginTop: 10 }}
                                    />
                                </View>
                            </View>
                            <View style={{alignItems: 'flex-end'}} >
                                <AppText
                                    text={`$${item?.price}`}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: colors.white, fontWeight: 'bold' }}
                                />
                                <View style={{alignItems: 'center', justifyContent: 'center',}} >
                                    <AppText
                                        text={`${item?.percentage}%`}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                        customStyle={{color: item?.bg_color  }}
                                    />
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 10,
    },
    banner: {
        padding: 20,
        borderRadius: 15,
        marginBottom: 20,
        width: '100%',
    },
})

export default MarketCard