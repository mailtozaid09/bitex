import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { media } from '../../../assets/media'
import AppText from '../../text'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import CustomIcon from '../../CustomIcon'
import { screenWidth } from '../../../utils/constants'
import {  bitcoin_data, } from '../../../global/sampleData'
import { useNavigation } from '@react-navigation/native'
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'


const PortfolioCard = ({onPress}) => {

    const navigation = useNavigation()

    const [data, setData] = useState(bitcoin_data.slice(0, 3));

    return (
        <View style={styles.container} >

            <View style={{marginVertical: 10, marginBottom: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                <AppText
                    text={"Portfolio"}
                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                    customStyle={{color: colors.white, fontSize: 20, fontFamily: fonts.primary_medium_font, fontWeight: '600'  }}
                />
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onPress}
                >
                <AppText
                    text={"View All+"}
                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                    customStyle={{color: '#363D4E', fontSize: 15, fontFamily: fonts.primary_medium_font, fontWeight: '600' }}
                />
                </TouchableOpacity>
            </View>

            <FlatList
                data={data}
                keyExtractor={item => item.id}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        onPress={() => {navigation.navigate(NAVIGATIONS.PORTFOLIO_DETAILS, {params: item})}}
                        key={index} style={[styles.banner, {backgroundColor: colors.bg_color}]} >

                        <View style={{marginBottom: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                <Image source={item?.icon} style={{height: 30, width: 30, marginRight: 15, resizeMode: 'contain'}} />
                                <AppText
                                    text={item?.title}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: '#A6A3B8', }}
                                />
                            </View>

                            <View style={{height: 30, borderRadius: 15, paddingHorizontal: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: item?.bg_color}} >
                                <AppText
                                    text={`${item?.percentage}%`}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                    customStyle={{color: colors.white  }}
                                />
                            </View>
                        </View>

                        <AppText
                            text={`$${item?.price}`}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: colors.white, fontWeight: 'bold' }}
                        />

                        <AppText
                            text={item?.subTitle}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: '#A6A3B8', marginTop: 10 }}
                        />
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 10,
    },
    banner: {
        padding: 20,
        borderRadius: 15,
        marginRight: 20,
        width: screenWidth - 100,
    },
})

export default PortfolioCard