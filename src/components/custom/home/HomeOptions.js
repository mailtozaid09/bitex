import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { media } from '../../../assets/media'
import AppText from '../../text'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import CustomIcon from '../../CustomIcon'
import { screenWidth } from '../../../utils/constants'
import { home_options } from '../../../global/sampleData'


const HomeOptions = ({onPress}) => {

    const [data, setData] = useState(home_options);

    return (
        <View style={styles.container} >
            {data?.map((item, index) => (
                <View style={{alignItems: 'center'}} >
                    <Image source={item?.icon} style={{height: 50, width: 50, resizeMode: 'contain'}} />
                    <AppText
                        text={item?.title}
                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                        customStyle={{color: colors.white, marginTop: 10, }}
                    />
                </View>
            ))}
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 20,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
})

export default HomeOptions