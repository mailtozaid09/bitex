import React from 'react'
import { Text, View, StyleSheet, TextInput } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import CustomIcon from '../CustomIcon'

const SearchBar = ({value, onChange}) => {
    return (
        <View style={styles.container} >
            <View style={styles.inputContainer} >
                <CustomIcon
                    iconName="search1"
                    iconType="AntDesign"
                    iconSize={24}
                    iconColor={colors.white}
                    iconContainer={{marginRight: 10,}}
                />
                <TextInput
                    placeholder='Search Coffee'
                    placeholderTextColor={colors.gray}
                    style={styles.input}
                    onChangeText={(text) => {onChange(text)}}
                    value={value}
                />
            </View>
            <CustomIcon
                iconName="filter-outline"
                iconType="Ionicons"
                iconSize={24}
                iconColor={colors.white}
                iconContainer={{height: 50, width: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: colors.primary}}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    inputContainer: {
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.black,
        marginRight: 10,
        paddingHorizontal: 10,
        borderRadius: 10,
    },
    input: {
        flex: 1,
        fontSize: 14,
        color: colors.white,
        fontFamily: fonts.primary_medium_font,
    },
    spaceBetween: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default SearchBar