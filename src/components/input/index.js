import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

import Icon from '../../utils/icons';
import { STRING_CONSTANTS, colors, fonts } from '../../utils';
import { media } from '../../assets/media';
import AppText from '../text';
import CustomIcon from '../CustomIcon';


const Input = ({value, label, customStyle, customInputStyle, inputIcon, placeholder, isPhoneNumber, error, isEditable, keyboardType, onChangeEyeIcon, showEyeIcon, onChangeText, clearTextValue, isPassword, isSearch }) => {
    return (
        <View style={[styles.container, customStyle]} >
            {label && <Text style={styles.inputLabel} >{label}</Text>}

            <View style={[styles.inputContainer, isEditable == false && {backgroundColor: colors.light_gray}]} >
            
                {isSearch && (
                    <View>
                        <Icon type="Feather" name="search" size={28} color={'#C0C9EA'} style={{marginRight: 12}}  />
                    </View>
                )}

                {inputIcon && (
                    <View>
                        <Image source={inputIcon} style={{height: 24, width: 24, resizeMode: 'contain', marginRight: 10}} />
                    </View>
                )}

                {isPhoneNumber && (
                    <View style={{width: 50, borderRightWidth: 1, borderColor: colors.gray, alignItems: 'flex-start', marginRight: 12}} >
                        <AppText
                            text={'+91'}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: colors.gray, textAlign: 'center'}}
                        />
                    </View>
                )}


                <TextInput
                    value={value}
                    placeholder={placeholder}
                    placeholderTextColor={"#C0C9EA"}
                    onChangeText={onChangeText}
                    style={[styles.input, customInputStyle]}
                    autoCorrect={false}
                    editable={isEditable}
                    autoCapitalize={label == 'Email' ? 'none' : null}
                    maxLength={isPhoneNumber ? 10 : null}
                    keyboardType={keyboardType ? keyboardType : isPhoneNumber ? 'number-pad' : 'default'}
                    secureTextEntry={isPassword && !showEyeIcon ? true : false}
                />


                {isEditable && (
                    <CustomIcon
                       iconName="pencil"
                       iconType="FontAwesome"
                       iconSize={20}
                       iconColor={colors.light_gray}
                   />
                )}

                {isPassword && (
                    <TouchableOpacity onPress={onChangeEyeIcon}>
                        
                        {showEyeIcon ? (
                            <CustomIcon
                                iconName="eye"
                                iconType="Feather"
                                iconSize={20}
                                iconColor={"#363D4E"}
                            />
                        ):(
                            <CustomIcon
                                iconName="eye-off"
                                iconType="Feather"
                                iconSize={20}
                                iconColor={"#363D4E"}
                            />
                        )}
                    </TouchableOpacity>
                )}


                {isSearch && value?.length != 0 && (
                    <TouchableOpacity onPress={clearTextValue}>
                        <Icon type="AntDesign" name="close" size={28} color={colors.black} />
                    </TouchableOpacity>
                )}
            </View>

            {error && <Text style={styles.error} >{error ? error : null}</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //width: '100%',
        //flex: 1,
        marginBottom: 20,
    },
    input: {
        fontSize: 16,
        lineHeight: 22,
        height: 54,
        fontFamily: fonts.primary_medium_font,
        flex: 1,
        marginRight: 10,
        color: colors.white,
    },
    inputContainer: {
        borderRadius: 28,
        // flex: 1,
        width: '100%', 
        height: 54,       
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        // borderWidth: 0.5,
        // borderColor: colors.gray,
        backgroundColor: colors.black,
    },
    inputLabel: {
        fontSize: 14,
        marginBottom: 4,
        color: colors.black,
        fontFamily: fonts.primary_semi_bold_font,
    },
    iconImage: {
        height: 26,
        width: 26
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 6,
        fontFamily: fonts.primary_medium_font,
        color: colors.red
    },

})

export default Input