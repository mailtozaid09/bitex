import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

import Icon from '../../utils/icons';
import { colors, fonts, media } from '../../utils';


const DarkInput = ({value, label, isLight, isFilter, showFilterSheet, placeholder, isPhoneNumber, error, editable, keyboardType, onChangeEyeIcon, showEyeIcon, onChangeText, clearTextValue, isPassword, isSearch }) => {
    return (
        <View style={[styles.container]} >
            {label && <Text style={styles.inputLabel} >{label}</Text>}

            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}} >
                <View style={{flex: 1, marginRight: 14, }} >
                    <View style={[styles.inputContainer, {}]} >
                        {isSearch && (
                            <View>
                                <Icon type="Feather" name="search" size={28} color={colors.white} style={{marginRight: 12}}  />
                            </View>
                        )}

                        <TextInput
                            value={value}
                            placeholder={placeholder}
                            placeholderTextColor={isLight ? colors.dark_gray : colors.light_gray}
                            onChangeText={onChangeText}
                            style={[styles.input, {color: isLight ? colors.black : colors.white,}]}
                            autoCorrect={false}
                            editable={editable}
                            maxLength={isPhoneNumber ? 10 : null}
                            keyboardType={keyboardType ? keyboardType : 'default'}
                            secureTextEntry={isPassword && !showEyeIcon ? true : false}
                        />

                        <View style={{}} >

                            {isPassword && (
                                <TouchableOpacity onPress={onChangeEyeIcon}>
                                    <Image source={showEyeIcon ? media.view : media.hide} style={styles.iconImage} />
                                </TouchableOpacity>
                            )}


                            {isSearch && value?.length != 0 && (
                                <TouchableOpacity onPress={clearTextValue}>
                                    <Icon type="AntDesign" name="close" size={28} color={colors.white} />
                                </TouchableOpacity>
                            )}
                        </View>

                    </View>
                </View>

                {isFilter && (
                    <TouchableOpacity 
                        onPress={showFilterSheet}
                        style={styles.filterContainer} >
                        <Icon type="MaterialCommunityIcons" name="sort" size={24} color={colors.white} />
                    </TouchableOpacity>
                )}
            </View>
            {error && <Text style={styles.error} >{error ? error : null}</Text>} 
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 4,
    },
    input: {
        fontSize: 16,
        lineHeight: 22,
        height: 54,
        fontFamily: fonts.primary_medium_font,
        flex: 1,
        
    },
    inputContainer: {
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: colors.gray,
    },
    inputLabel: {
        fontSize: 14,
        marginVertical: 4,
        color: colors.white,
        fontFamily: fonts.primary_semi_bold_font,
    },
    iconImage: {
        height: 26,
        width: 26
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 6,
        fontFamily: fonts.primary_medium_font,
        color: colors.red
    },
    filterContainer: {
        height: 54,
        width: 54,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    }

})

export default DarkInput