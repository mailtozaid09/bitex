import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors } from '../../utils'
 

const Loader = ({loaderColor, size}) => {
    return (
        <View style={{flex: 1, alignItems: 'center',justifyContent: 'center'}} >
            <ActivityIndicator size="large" color={loaderColor ? loaderColor : colors.black} />
        </View>
    )
}

export default Loader