import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { media } from '../../assets/media'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import AppText from '../text'

const HomeHeader = ({title, onMenuPress, onBellPress}) => {
    return (
        <View style={styles.container} >
            <View style={styles.headerContainer} >
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onMenuPress}
                >
                    <Image source={media.hamburger} style={styles.hamburger} />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onBellPress}
                    style={{height: 50, width: 50, backgroundColor: colors.primary, alignContent: 'center', justifyContent: 'center', borderRadius: 8}}
                >
                    <Image source={media.notification} style={styles.notification} />
                </TouchableOpacity>
            </View>

            <View style={{paddingHorizontal: 20, paddingVertical: 10 }} >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.HEADING}
                    customStyle={{color: colors.sea_green, fontSize: 20, fontFamily: fonts.primary_medium_font, fontWeight: '600' }}
                />
                    <AppText
                    text={'Good Morning'}
                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                    customStyle={{color: colors.white, marginTop: 8, fontSize: 24, fontFamily: fonts.primary_bold_font, fontWeight: 'bold' }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: colors.black,
    },
    headerContainer: {
        height: 60,
        width: '100%',
        paddingHorizontal: 14,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    hamburger: {
        height: 28,
        width: 28,
        resizeMode: 'contain',
    },
    notification: {
        height: 28,
        width: 50,
        resizeMode: 'contain',
    }
})

export default HomeHeader