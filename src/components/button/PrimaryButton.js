import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

import Loader from '../loader'
import { colors, fonts } from '../../utils'
 

const PrimaryButton = ({title, disabled, buttonLoader, buttonStyle, textColor, backgroundColor, loaderColor, onPress}) => {

    return (
        <View style={[styles.container, buttonStyle]} >
            {title != 'Delete'
            ?
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.button, {backgroundColor: disabled ? colors.gray : backgroundColor ? backgroundColor: colors.primary}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={loaderColor} /> : <Text style={[styles.title, {color: textColor ? textColor : colors.black}]} >{title}</Text>}
            </TouchableOpacity>
            :
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.deleteButton, {backgroundColor: disabled ? colors.gray : backgroundColor ? backgroundColor : colors.white}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={colors.red} /> : <Text style={styles.deleteTitle} >{title}</Text>}
            </TouchableOpacity>
            }
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 14,
    },
    button: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
    },
    deleteButton: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: colors.red,
    },
    title: {
        fontSize: 16,
        fontFamily: fonts.primary_semi_bold_font,
        color: colors.white,
    },
    deleteTitle: {
        fontSize: 16,
        fontFamily: fonts.primary_medium_font,
        color: colors.red,
    }
})

export default PrimaryButton