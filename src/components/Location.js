import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity,} from 'react-native'
import AppText from './text'
import { STRING_CONSTANTS, colors } from '../utils'


const Location = ({  }) => {
    return (
        <View>
            <AppText
                text={"Location"}
                type={STRING_CONSTANTS.textConstants.LABEL}
                customStyle={{ color: colors.gray, }}
            />
            <AppText
                text={"Bangalore, India"}
                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                customStyle={{ color: colors.white, }}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        
    }
})

export default Location