import { media } from "../assets/media"


export const onboarding_data = [
    {
        id: 1,
        image: media.onboarding_1,
        title: 'Welcome to Cryptocurrency',
        description: 'Experience the future of finance through blockchain technology.',
    },
    {
        id: 2,
        image: media.onboarding_2,
        title: 'Buy & Sell \nBitcoin',
        description: 'Easily buy and sell Bitcoin anytime, anywhere, with our user-friendly platform.'
    },
    {
        id: 3,
        image: media.onboarding_3,
        title: 'Explore Cryptocurrency Market',
        description: 'Access real-time data and insights to make informed investment decisions.'
    },
]


export const banner_data = [
    {
        id: 1,
        wallet_balance: '705874.40',
        weekly_profit: '1580.78',
        percentage: '15',
        bg_color: '#34D9D1'
    },
    {
        id: 2,
        wallet_balance: '105874.40',
        weekly_profit: '280.78',
        percentage: '5',
        bg_color: '#F8A946'
    },
    {
        id: 3,
        wallet_balance: '1205874.40',
        weekly_profit: '3580.78',
        percentage: '35',
        bg_color: '#FF73AA'
    },
]


export const bitcoin_data = [
    {
        id: 1,
        icon: media.market_1,
        title: 'Achain',
        subTitle: 'ACH',
        price: '15380.78',
        percentage: '+2.40',
        bg_color: '#FF73AA', 
    },
    {
        id: 2,
        icon: media.market_2,
        title: 'Tron',
        subTitle: 'TRX',
        price: '28330.78',
        percentage: '-0.40',
        bg_color: '#FF73AA',
    },
    {
        id: 3,
        icon: media.market_3,
        title: 'Ethereum',
        subTitle: 'BCH',
        price: '35380.78',
        percentage: '+2.40',
        bg_color: '#34D9D1',
    },
    {
        id: 4,
        icon: media.market_4,
        title: 'Binance Coin',
        subTitle: 'CAD',
        price: '28330.78',
        percentage: '-0.40',
        bg_color: '#FF73AA',
    },
    {
        id: 5,
        icon: media.market_5,
        title: 'Pound',
        subTitle: 'GBP',
        price: '35380.78',
        percentage: '+2.40',
        bg_color: '#F8A946',
    },
    {
        id: 6,
        icon: media.market_6,
        title: 'Tether',
        subTitle: 'USDT',
        price: '28330.78',
        percentage: '-0.40',
        bg_color: '#FF73AA',
    },
    {
        id: 7,
        icon: media.market_7,
        title: 'Bitcoin Cash',
        subTitle: 'BCH',
        price: '35380.78',
        percentage: '+2.40',
        bg_color: '#34D9D1',
    },
]

export const bitcoin_sub_data = [
    {
        title: 'Market Cap',
        value: '20,351.00 BTC'
    },
    {
        title: 'Volume (24 hours)',
        value: '$98,669.59'
    },
    {
        title: 'Available Supply',
        value: '15.897.198'
    },
    {
        title: 'Total Supply',
        value: '27.6412.348'
    },
    {
        title: 'Low (24 hours)',
        value: '14,987.12'
    },
    {
        title: 'High (24 hours)',
        value: '54,147.96'
    },
]

export const portfolio_data = [
    {
        id: 1,
        icon: media.market_1,
        title: 'Ethereum',
        subTitle: 'ETH',
        price: '15380.78',
        percentage: '+2.40',
        bg_color: '#34D9D1'
    },
    {
        id: 2,
        icon: media.market_2,
        title: 'Bitcoin',
        subTitle: 'BTC',
        price: '28330.78',
        percentage: '-0.40',
        bg_color: '#FF73AA',
    },
    {
        id: 3,
        icon: media.market_3,
        title: 'Ethereum',
        subTitle: 'ETH',
        price: '35380.78',
        percentage: '+2.40',
        bg_color: '#F8A946',
    },
]

export const password_validations = [
    { title: 'At least 8 characters', isValid: false },
    { title: 'Contains a number', isValid: false },
    { title: 'Contains an uppercase letter', isValid: false },
]


export const notification_data = [
    {
        title: 'ETH received',
        description: '0.08 ETH Received',
        date: '1 days ago',
        icon: media.notification_1,
    },
    {
        title: 'Payment',
        description: 'Thank you! Your transaction is completed',
        date: '2 days ago',
        icon: media.notification_2,
    },
    {
        title: 'Promotion',
        description: 'Invite friends - Get 1 coupons each!',
        date: '4 days ago',
        icon: media.notification_3,
    },
    {
        title: 'New Coin',
        description: 'New bid 0.2 ETH',
        date: '5 days ago',
        icon: media.notification_4,
    },
    {
        title: 'Payment',
        description: 'Thank you! Your transaction is completed',
        date: '5 days ago',
        icon: media.notification_5,
    },
]


export const profile_options = [
    {
        title: 'Change Password',
        icon: media.change_password,
    },
    {
        title: 'Order Management',
        icon: media.order_management,
    },
    {
        title: 'Document Management',
        icon: media.document_management,
    },
    {
        title: 'Payment',
        icon: media.payment,
    },
    {
        title: 'Sign Out',
        icon: media.sign_out,
    },
]


export const profile_more_options = [
    {
        title: 'Newsletter',
        icon: media.news_letter,
        rightSide: 'switch',
    },
    {
        title: 'Text Message',
        icon: media.text_message,
        rightSide: 'switch',
    },
    {
        title: 'Phone Call',
        icon: media.phone_call,
        rightSide: 'switch',
    },
    {
        title: 'Currency',
        icon: media.currency,
        rightSide: '$USD',
    },
    {
        title: 'Language',
        icon: media.language,
        rightSide: 'English',
    },
    {
        title: 'Linked Accounts',
        icon: media.links,
        rightSide: 'Facebook, google',
    },
]

export const home_options = [
    {
        title: 'Send',
        icon: media.send,
    },
    {
        title: 'Receive',
        icon: media.receive,
    },
    {
        title: 'Buy',
        icon: media.buy,
    },
    {
        title: 'Swap',
        icon: media.swap,
    },
]
