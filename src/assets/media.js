export const media = {
    home_fill: require('../assets/icons/tabbar/home_fill.png'),
    card_fill: require('../assets/icons/tabbar/card_fill.png'),
    chat_fill: require('../assets/icons/tabbar/chat_fill.png'),
    settings_fill: require('../assets/icons/tabbar/settings_fill.png'),
   
    home: require('../assets/icons/tabbar/home.png'),
    card: require('../assets/icons/tabbar/card.png'),
    chat: require('../assets/icons/tabbar/chat.png'),
    settings: require('../assets/icons/tabbar/settings.png'),
    add: require('../assets/icons/tabbar/add.png'),

    app_icon: require('../assets/icons/app_icon.png'),

    hamburger: require('../assets/icons/hamburger.png'),
    search: require('../assets/icons/search.png'),
    user: require('../assets/icons/user.png'),
    notification: require('../assets/icons/notification.png'),

    change_password: require('../assets/icons/change_password.png'),
    order_management: require('../assets/icons/order_management.png'),
    document_management: require('../assets/icons/document_management.png'),
    payment: require('../assets/icons/payment.png'),
    sign_out: require('../assets/icons/sign_out.png'),
    news_letter: require('../assets/icons/news_letter.png'),
    text_message: require('../assets/icons/text_message.png'),
    phone_call: require('../assets/icons/phone_call.png'),
    currency: require('../assets/icons/currency.png'),
    language: require('../assets/icons/language.png'),
    links: require('../assets/icons/links.png'),

    email: require('../assets/icons/email.png'),
    email_disabled: require('../assets/icons/email_disabled.png'),
    password: require('../assets/icons/password.png'),

    expand: require('../assets/icons/expand.png'),
    candle_icon: require('../assets/icons/candle_icon.png'),

    
    // home
    send: require('../assets/icons/home/send.png'),
    receive: require('../assets/icons/home/receive.png'),
    buy: require('../assets/icons/home/buy.png'),
    swap: require('../assets/icons/home/swap.png'),


    // market
    market_1: require('../assets/icons/market/market_1.png'),
    market_2: require('../assets/icons/market/market_2.png'),
    market_3: require('../assets/icons/market/market_3.png'),
    market_4: require('../assets/icons/market/market_4.png'),
    market_5: require('../assets/icons/market/market_5.png'),
    market_6: require('../assets/icons/market/market_6.png'),
    market_7: require('../assets/icons/market/market_7.png'),

    // notifications
    notification_1: require('../assets/icons/notifications/notification_1.png'),
    notification_2: require('../assets/icons/notifications/notification_2.png'),
    notification_3: require('../assets/icons/notifications/notification_3.png'),
    notification_4: require('../assets/icons/notifications/notification_4.png'),
    notification_5: require('../assets/icons/notifications/notification_5.png'),

    onboarding_1: require('../assets/images/onboarding/onboarding_1.png'),
    onboarding_2: require('../assets/images/onboarding/onboarding_2.png'),
    onboarding_3: require('../assets/images/onboarding/onboarding_3.png'),

    email_login: require('../assets/images/login/email_login.png'),
    number_login: require('../assets/images/login/number_login.png'),
    otp_verification: require('../assets/images/login/otp_verification.png'),
    create_password: require('../assets/images/login/create_password.png'),
    forgot_password: require('../assets/images/login/forgot_password.png'),
    location: require('../assets/images/login/location.png'),

    graph: require('../assets/images/graph.png'),
    view: require('../assets/images/view.png'),
    hide: require('../assets/images/hide.png'),
    portfolio_bg: require('../assets/images/portfolio_bg.png'),

}
