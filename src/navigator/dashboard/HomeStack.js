import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

import { HomeScreen, NotificationScreen } from '../../screens/dashboard';
import MarketTrends from '../../screens/dashboard/home/MarketTrends';
import PortfolioScreen from '../../screens/dashboard/home/PortfolioScreen';
import PortfolioDetails from '../../screens/dashboard/home/PortfolioDetails';


const Stack = createStackNavigator();

const HomeStack = ({navigation}) => {

    return (
        <Stack.Navigator 
            initialRouteName={NAVIGATIONS.HOME}
        >
            <Stack.Screen
                name={NAVIGATIONS.HOME}
                component={HomeScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NAVIGATIONS.NOTIFICATION}
                component={NotificationScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name={NAVIGATIONS.PORTFOLIO}
                component={PortfolioScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NAVIGATIONS.MARKET}
                component={MarketTrends}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NAVIGATIONS.PORTFOLIO_DETAILS}
                component={PortfolioDetails}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default HomeStack