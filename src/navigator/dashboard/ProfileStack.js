import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProfileScreen } from '../../screens/dashboard';

import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

const Stack = createStackNavigator();

const ProfileStack = ({navigation}) => {

    return (
        <Stack.Navigator 
            initialRouteName={NAVIGATIONS.PROFILE}
        >
            <Stack.Screen
                name={NAVIGATIONS.PROFILE}
                component={ProfileScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default ProfileStack