import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen } from '../../screens/dashboard';

import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

const Stack = createStackNavigator();

const CardStack = ({navigation}) => {

    return (
        <Stack.Navigator 
            initialRouteName={NAVIGATIONS.HOME}
        >
            <Stack.Screen
                name={NAVIGATIONS.HOME}
                component={HomeScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default CardStack