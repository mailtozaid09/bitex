import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen, AddScreen } from '../../screens/dashboard';

import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

const Stack = createStackNavigator();

const AddStack = ({navigation}) => {

    return (
        <Stack.Navigator 
            initialRouteName={NAVIGATIONS.HOME}
        >
            <Stack.Screen
                name={NAVIGATIONS.HOME}
                component={HomeScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NAVIGATIONS.ADD}
                component={AddScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default AddStack