import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { ProfileScreen, SettingsScreen } from '../../screens/dashboard';

import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

const Stack = createStackNavigator();

const SettingsStack = ({navigation}) => {

    return (
        <Stack.Navigator 
            initialRouteName={NAVIGATIONS.HOME}
        >
            <Stack.Screen
                name={NAVIGATIONS.SETTINGS}
                component={SettingsScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name={NAVIGATIONS.PROFILE}
                component={ProfileScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default SettingsStack