import React, {useState, useEffect} from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import AsyncStorage from '@react-native-async-storage/async-storage';

import OnboardingScreen from '../../screens/onboarding';
import GetStarted from '../../screens/onboarding/GetStarted';

import { ForgotPassword, LoginScreen, SignupScreen, VerifyOtp, CreatePassword } from '../../screens/auth';

import LoadingScreen from '../../screens/onboarding/Loading';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();

const LoginStack = ({navigation}) => {

    const onborading_done = useSelector(state => state.auth.onborading_done);

    return (
        <Stack.Navigator 
            initialRouteName={onborading_done != null ? "GetStarted" : "Loading"}
        >
            
            <Stack.Screen
                name="Loading"
                component={LoadingScreen}
                options={{
                    headerShown: false,
                }}
            />
            
            <Stack.Screen
                name="GetStarted"
                component={GetStarted}
                options={{
                    headerShown: false,
                }}
            />
            
            <Stack.Screen
                name="Onboarding"
                component={OnboardingScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="Signup"
                component={SignupScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="VerifyOtp"
                component={VerifyOtp}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="CreatePassword"
                component={CreatePassword}
                options={{
                    headerShown: false,
                }}
            />
            
            <Stack.Screen
                name="ForgotPassword"
                component={ForgotPassword}
                options={{
                    headerShown: false,
                }}
            />

        </Stack.Navigator>
    );
}

export default LoginStack