import React, {useState, useEffect, useContext} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';

import { useDispatch, useSelector } from 'react-redux';

import LoginStack from '../auth/LoginStack';
import Tabbar from '../tabbar';


const Stack = createStackNavigator();


const Navigator = ({}) => {

    const dispatch = useDispatch()

    const navigation = useNavigation()

    const auth_token = useSelector(state => state.auth.auth_token);
    console.log("auth_token > ", auth_token);

    
    useEffect(() => {
        console.log("Navigator > > ");
    }, [])
    

    return (
        <>
        <Stack.Navigator 
            initialRouteName={auth_token != null ? 'Tabbar' : 'LoginStack'}  
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>

        </>
    );
}

export default Navigator