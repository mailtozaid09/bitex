import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { media } from '../../assets/media';

import { AddStack, CardStack, ChatStack, HomeStack, ProfileStack, SettingsStack } from '../dashboard';

import { colors } from '../../utils';
import { NAVIGATIONS } from '../../utils/constants/navigationConstants';



const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {

    LogBox.ignoreAllLogs(true)
 
    
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = focused ? 30 : 26, width = focused ? 30 : 26 ;
    
                if (route.name === NAVIGATIONS.HOME_STACK) {
                    iconName = focused ? media.home_fill : media.home
                    routeName = NAVIGATIONS.HOME
                }
                else if (route.name === NAVIGATIONS.CARD_STACK) {
                    iconName = focused ? media.card_fill : media.card
                    routeName = NAVIGATIONS.CARD
                } 
                else if (route.name === NAVIGATIONS.CHAT_STACK) {
                    iconName = focused ? media.chat_fill : media.chat
                    routeName = NAVIGATIONS.CHAT
                } 
                else if (route.name === NAVIGATIONS.SETTINGS_STACK) {
                    iconName = focused ? media.settings_fill : media.settings
                    routeName = NAVIGATIONS.SETTINGS
                } 

                return(
                    <View style={{alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={iconName} style={{height: height, width: width, resizeMode: 'contain'}}/>
                        
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name={NAVIGATIONS.HOME_STACK}
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === NAVIGATIONS.MARKET || routeName === NAVIGATIONS.PORTFOLIO || routeName === NAVIGATIONS.PORTFOLIO_DETAILS || routeName === NAVIGATIONS.NOTIFICATION) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name={NAVIGATIONS.CARD_STACK}
                component={CardStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === NAVIGATIONS.PRODUCT_DETAILS) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

        <Tab.Screen
                name={NAVIGATIONS.ADD_STACK}
                component={AddStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarButton: (props) => (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            onPress={() => { navigation.navigate(NAVIGATIONS.ADD_STACK, {screen: NAVIGATIONS.ADD}); }}
                            style={{width: 80, alignItems: 'center', justifyContent: 'center',}}
                        >
                            <Image source={media.add} style={{height: 50, width: 50, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                    ),
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === NAVIGATIONS.ADD) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                    })(route)
                })}
            />


            <Tab.Screen
                name={NAVIGATIONS.CHAT_STACK}
                component={ChatStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === NAVIGATIONS.PROFILE_STACK) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name={NAVIGATIONS.SETTINGS_STACK}
                component={SettingsStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === NAVIGATIONS.PROFILE) {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 0,
        borderColor: colors.gray, 
        backgroundColor: colors.bg_color,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        position: 'absolute',
        
    },
    addButtonContainer: {
        height: 60,
        width: 60, 
        borderRadius: 30,
        borderWidth: 0.1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 6,
    }
})