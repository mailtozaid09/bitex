export default colors = {
    primary: '#191C26',
    primary_dark: '#191C26',
    secondary: '#363D4E',

    sea_green: '#58E3DC',

    bg_color: '#1E2230',
    
    black: '#111319',
    white: '#fff',
    green: '#007c02',
    dark_gray: '#3d3d3d',
    yellow: '#ffb32d',

    dark_purple: '#261d45',

    brown: '#EA9F5F',
    red: '#ee534f',

    cream: '#E8B88D',
    light_cream: '#F0E3D5',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    

    
    gray: '#919399',
    light_gray: '#EFF0F6',
    
    blue: '#81b3f3',

    orange: '#f7c191',
    reddish: '#ED4545',
}
