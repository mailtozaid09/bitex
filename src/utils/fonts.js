
const primary_font = 'Roboto'

export default fonts =  {
    primary_bold_font: `${primary_font}-Bold`,
    primary_semi_bold_font: `${primary_font}-SemiBold`, 
    primary_medium_font: `${primary_font}-Medium`,
    primary_regular_font: `${primary_font}-Regular`,
    primary_light_font: `${primary_font}-Light`,
    primary_thin_font: `${primary_font}-Thin`,
}
