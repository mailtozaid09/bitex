export const NAVIGATIONS = {
    HOME: 'Home',
    CARD: 'Home',
    CHAT: 'Home',
    SETTINGS: 'Settings',
    PROFILE: 'Profile',
    ADD: 'Add',
    
    TABBAR: 'Tabbar',

    NOTIFICATION: 'Notification',
    MARKET: 'Market',
    PORTFOLIO: 'Portfolio',
    PORTFOLIO_DETAILS: 'PortfolioDetails',
    

    HOME_STACK: 'HomeStack',
    CARD_STACK: 'CardStack',
    CHAT_STACK: 'ChatStack',
    SETTINGS_STACK: 'SettingsStack',
    PROFILE_STACK: 'ProfileStack',
    LOGIN_STACK: 'LoginStack',
    ADD_STACK: 'AddStack',


    LOADING: 'Loading',
    GET_STARTED: 'GetStarted',

    LOGIN: 'Login',
    SIGN_UP: 'Signup',
    VERIFY_OTP: 'VerifyOtp',
    FORGOT_PASSWORD: 'ForgotPassword',
    CREATE_PASSWORD: 'CreatePassword',
}