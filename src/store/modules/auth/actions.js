
import { 
    CURRENT_USER_DETAILS, 
    SET_AUTH_TOKEN,
    SET_ONBOARDING_DONE,
} from './actionTypes';

let nextUserId = 0;

export function setAuthToken(auth_token) {
    return {
        type: SET_AUTH_TOKEN,
        payload: {
			auth_token,
		},
    };
};

export function setOnboardingDetails(onborading_done) {
    console.log("onborading_done>> ",onborading_done);
    return {
        type: SET_ONBOARDING_DONE,
        payload: {
			onborading_done,
		},
    };
};

export function currentUserDetails(params) {
  return {
      type: CURRENT_USER_DETAILS,
      payload: {
          id: params.id,
          name: params.name,
          email: params.email,
          profile_url: params.profile_url,
          phone: params.phone,
          isAdmin: params.isAdmin,
      },
  };
};

