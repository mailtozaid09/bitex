import { CURRENT_USER_DETAILS, SET_AUTH_TOKEN, SET_ONBOARDING_DONE } from "./actionTypes";

const INITIAL_STATE = {
    auth_token: null,
    onborading_done: null,

    current_user_details: null,
};

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        
        case SET_AUTH_TOKEN: {
            return {
                ...state,
                auth_token: action.payload.auth_token,
            };
        }

        case SET_ONBOARDING_DONE: {
            return {
                ...state,
                onborading_done: action.payload.onborading_done,
            };
        }

        
        case CURRENT_USER_DETAILS: {
            const {
                id, name, email, profile_url, phone, isAdmin,
            } = action.payload
            
                return {
                    ...state,
                    current_user_details: {
                        id, name, email, profile_url, phone, isAdmin,
                    }
            };
        }
        
        default:
            return state;
        }
}
