import React, { useContext, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, } from 'react-native'

import Input from '../../../components/input'
import { useDispatch } from 'react-redux'
import { setAuthToken, userLoggedIn } from '../../../store/modules/auth/actions'
import UseLoader from '../../../components/loader/useLoader'
import { ToastContext } from '../../../context/ToastContext'

import AppText from '../../../components/text'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'

import { AlreadyHaveAccount, ForgotPassword, RememberMe } from '../../../components/custom/login'

import { media } from '../../../assets/media'

import LinearGradient from 'react-native-linear-gradient'

import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'
import { CustomHeader } from '../../../components/headers'

const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [form, setForm] = useState({
        email: 'test@gmail.com',
        password: 'Test@123'
    })
    const [errors, setErrors] = useState({});

    const [showEyeIcon, setShowEyeIcon] = useState(false);
 
    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);

    const [toggleCheckBox, setToggleCheckBox] = useState(false)

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    const loginFunction = () => {
        showLoader();
    
        if(checkForValidations()){
            console.log("Login details > > > ", form);
            var body = {
                "email": form.email,
                "password": form.password,
            }
    
            console.log("body=> ", body);
            loginUserFunction(body)
        }else{
            hideLoader()
        }
    }


    const checkForValidations = () => {
        var isValid = true;

        if(!form.password){
            console.log("Please enter a valid password!");
            isValid = false
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password!'}
            })
        }

        if(!form.email){
            console.log("Please enter a valid email address!");
            isValid = false
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email address!'}
            })
        }

        return isValid
    }


    const loginUserFunction = async (body) => {
        console.log("body> ",body);
        
        let user = {
            email: 'test@gmail.com',
            password: 'Test@123'
        }

        if(body?.email == user?.email && body?.password == user?.password){
            console.log("success");
            dispatch(setAuthToken('auth_token'))
            navigation.navigate(NAVIGATIONS.TABBAR)
        }else{
            setErrors((prev) => {
                return {...prev, login: 'Please enter valid credentials!'}
            })
        }
    };

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Sign In"} isBack />

            <ScrollView style={styles.mainContainer} >
                
                <View style={styles.imageContainer} >
                    <Image source={media.email_login} style={styles.loginImg} />
                </View>

                <Input
                    placeholder="Enter your email"
                    value={form.email}
                    error={errors.email}
                    inputIcon={media.email}
                    onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                />

                <Input
                    placeholder="Enter your password"
                    isPassword
                    showEyeIcon={showEyeIcon}
                    error={errors.password}
                    inputIcon={media.password}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    value={form.password}
                    onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                />

                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                    <RememberMe
                        toggleCheckBox={toggleCheckBox}
                        onToggle={(val) => setToggleCheckBox(val)}
                        onPress={() => {navigation.navigate('ForgotPassword')}}
                    />
                    <ForgotPassword
                        onPress={() => {navigation.navigate(NAVIGATIONS.FORGOT_PASSWORD)}}
                    />
                </View>

                <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                            loginFunction()
                        }} 
                    >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.loginButton, {marginVertical: 20}]}
                        >
                            <AppText
                                text={'Sign In'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                </TouchableOpacity>

                {errors?.login && (<AppText
                        text={errors?.login}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: colors.red, textAlign: 'center', marginTop: 4, marginBottom: 10}}
                    />)}

                <AlreadyHaveAccount
                    title="Don't have an account?"
                    buttonText="Sign Up"
                    buttonColor={colors.white}
                    signColor="#FDAE67"
                    onPress={() => {navigation.navigate('Signup')}}
                />

            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        width: '100%', 
        padding: 20,
        paddingHorizontal: 20, 
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center', 
        marginVertical: 20,
    },
    loginImg: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },


})

export default LoginScreen