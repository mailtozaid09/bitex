import React, { useContext, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, } from 'react-native'


import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'
import { useDispatch } from 'react-redux'
import { userLoggedIn } from '../../../store/modules/auth/actions'
import UseLoader from '../../../components/loader/useLoader'
// import { signupUser } from '../../../services/apiRequests'
import { ToastContext } from '../../../context/ToastContext'
import AppText from '../../../components/text'
import { STRING_CONSTANTS, colors } from '../../../utils'

import { media } from '../../../assets/media'
import { AlreadyHaveAccount } from '../../../components/custom/login'
import LinearGradient from 'react-native-linear-gradient'
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'
import { CustomHeader } from '../../../components/headers'

const SignupScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});

    const [showEyeIcon, setShowEyeIcon] = useState(false);
 
    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    const signupFunction = () => {
        console.log("Dsad");
        //dispatch(userLoggedIn(true))
        //navigation.navigate('Tabbar')
        showLoader();
    
        if(checkForValidations()){
            console.log("Login details> > > ", form);
            var body = {
                "number": form.number,
            }
            signupUserFunction(body)
        }else{
            hideLoader()
        }
    }


    const checkForValidations = () => {
        var isValid = true;
        
        if(!form.number || form?.number?.length != 10){
            console.log("Please enter a valid mobile number!");
            isValid = false
            setErrors((prev) => {
                return {...prev, number: 'Please enter a valid mobile number!'}
            })
        }


        return isValid
    }


    const signupUserFunction = async (body) => {
        navigation.navigate(NAVIGATIONS.VERIFY_OTP, {params: form.number})
        // const onSuccess = (resp) => {
        //     console.log("resp > ",resp);
        //     hideLoader();
        //     showToast(resp?.message, true);

        //     navigation.goBack()
        // };
        // const onError = (err) => {
        //     console.log("err > ",err);
        //     hideLoader();
        //     showToast(err, false);
        // };
        // signupUser(
        //     body,
        //     onSuccess,
        //     onError
        // );
    };

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Sign Up"} isBack />
            <ScrollView style={styles.mainContainer} >
                
                <View style={styles.imageContainer} >
                    <Image source={media.number_login} style={styles.loginImg} />
                </View>

                <AppText
                    text={'Simply enter your phone number to login or create an account.'}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                />

                <Input
                    placeholder="Enter your mobile number"
                    value={form.number}
                    isPhoneNumber
                    error={errors.number}
                    onChangeText={(text) => {onChange({name: 'number', value: text,}); setErrors({}); }}
                />


                <AppText
                    text={'By using our mobile app, you agree to our Privacy Policy and Terms of Use'}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                />

                <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                            signupFunction()
                        }} 
                    >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.loginButton, {marginVertical: 20}]}
                        >
                            <AppText
                                text={'Continue'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </TouchableOpacity>

                <AlreadyHaveAccount
                    title="Already have an account?"
                    buttonText="Sign In"
                    buttonColor={colors.white}
                    signColor="#FDAE67"
                    onPress={() => {navigation.navigate('Login')}}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        width: '100%', 
        padding: 20, 
        paddingHorizontal: 20,
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center', 
        marginVertical: 20,
    },
    loginImg: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

})

export default SignupScreen