import React, { useRef, useEffect, useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ScrollView, SafeAreaView, Alert, Image, TouchableOpacity } from 'react-native';

import { STRING_CONSTANTS, colors, fonts } from '../../utils';

import AppText from '../../components/text';
import { PrimaryButton,  } from '../../components/button';
import { CustomHeader } from '../../components/headers';
import { media } from '../../assets/media';
import LinearGradient from 'react-native-linear-gradient';
import ResendCode from '../../components/custom/login/ResendCode';
import { NAVIGATIONS } from '../../utils/constants/navigationConstants';

const VerifyOtp = (props) => {

    const navigation = props?.navigation

    const [number, setNumber] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [otp, setOtp] = useState('');
    const [error, setError] = useState('');
    const refs = useRef([]);

    useEffect(() => {
        let details = '+91 ' + props?.route?.params?.params
        setNumber(details)
        setPhoneNumber(props?.route?.params?.params)
    }, [])
    
  
    const handleOTPChange = (index, value) => {
        const newOtp = otp.split('');
        newOtp[index] = value;
        setOtp(newOtp.join(''));
        
        setError('')

        if (value && index < 3) {
            refs.current[index + 1].focus();
        }
    };
  
    const handleVerifyOTP = () => {
     
        if(otp?.length < 4){
            setError("Please enter a valid otp number!")
        }else{
            verifyOtp()
        }
    };

    const verifyOtp = () => {
        const correctOTP = '1234'; 
        if (otp === correctOTP) {
            navigation.navigate(NAVIGATIONS.CREATE_PASSWORD, {params: phoneNumber})
        } else {
            setOtp('')
            setError("Invalid OTP!")
        }
    }
  
    return (
        <>
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"OTP Verification"} isBack />
            <ScrollView style={styles.mainContainer} >
                
                <View style={styles.imageContainer} >
                    <Image source={media.number_login} style={styles.loginImg} />
                </View>

                <AppText
                    text={`An authentication code has been sent to ${number}`}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginTop: 20}}
                />


                <View style={styles.otpContainer} >
                    {Array.from({ length: 4 }).map((_, index) => (
                        <TextInput
                            key={index}
                            style={styles.input}
                            ref={ref => (refs.current[index] = ref)}
                            maxLength={1}
                            keyboardType="numeric"
                            value={otp[index] || ''}
                            onChangeText={value => handleOTPChange(index, value)}
                        />
                    ))}
                </View>

                {error && (<AppText
                        text={error}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: colors.red, textAlign: 'center', marginTop: 4, marginBottom: 10}}
                    />)}

                <ResendCode
                    title={"I didn't receive code."}
                    onPress={() => {}}
                />

                <View style={styles.timeLeftContainer} >
                    <View style={{width: 60}} >
                        <AppText
                            text={'1:20'}
                            type={STRING_CONSTANTS.textConstants.CAPTION1}
                            customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: colors.sea_green, textAlign: 'center', marginTop: 4, marginBottom: 20}}
                        />
                    </View>
                    <AppText
                        text={'Sec Left'}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: colors.sea_green, textAlign: 'center', marginTop: 4, marginBottom: 20}}
                    />
                </View>



                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        handleVerifyOTP()
                    }} 
                >
                    <LinearGradient
                        colors={['#8DF3ED', '#34D9D1']}
                        style={[styles.loginButton, {marginVertical: 20}]}
                    >
                        <AppText
                            text={'Verify Now'}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: colors.white, textAlign: 'center'}}
                        />
                    </LinearGradient>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>

   
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        width: '100%', 
        padding: 20, 
        paddingHorizontal: 20,
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center', 
        marginVertical: 20,
    },
    loginImg: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    timeLeftContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    otpContainer: {
        marginVertical: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        width: 60,
        height: 60,
        color: colors.white,
        fontFamily: fonts.primary_regular_font,
        fontWeight: '500',
        fontSize: 18,
        backgroundColor: colors.black,
        borderRadius: 30,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
});


export default VerifyOtp
