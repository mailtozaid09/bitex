import React, { useRef, useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ScrollView, SafeAreaView, Alert, Image, TouchableOpacity } from 'react-native';

import { STRING_CONSTANTS, colors, fonts } from '../../utils';

import AppText from '../../components/text';
import { PrimaryButton,  } from '../../components/button';
import { CustomHeader } from '../../components/headers';
import { media } from '../../assets/media';
import LinearGradient from 'react-native-linear-gradient';

import Input from '../../components/input';
import { NAVIGATIONS } from '../../utils/constants/navigationConstants';


const ForgotPassword = ({navigation}) => {
    
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }
  
    return (
        <>
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Forgot Password"} isBack />
            <ScrollView style={styles.mainContainer} >
                
                <View style={styles.imageContainer} >
                    <Image source={media.create_password} style={styles.loginImg} />
                </View>

                <AppText
                    text={'We will send a mail to the email address you registered to regain your password'}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                />

                <Input
                    placeholder="johndoe@mail.com"
                    value={form.email}
                    error={errors.email}
                    inputIcon={media.email_disabled}
                    onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                />

                <View>
                
                    <AppText
                        text={'Email sent to ex*****@gmail.com'}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#FE8270', textAlign: 'center', marginVertical: 20}}
                    />


                </View>

                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        navigation.navigate(NAVIGATIONS.CREATE_PASSWORD)
                    }} 
                >
                    <LinearGradient
                        colors={['#8DF3ED', '#34D9D1']}
                        style={[styles.loginButton, {marginVertical: 20}]}
                    >
                        <AppText
                            text={'Send'}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: colors.white, textAlign: 'center'}}
                        />
                    </LinearGradient>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>

   
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        width: '100%', 
        padding: 20, 
        paddingHorizontal: 45,
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center', 
        marginVertical: 20,
    },
    loginImg: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    passwordValidations: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8, 
    }
});

export default ForgotPassword