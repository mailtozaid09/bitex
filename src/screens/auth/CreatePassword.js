import React, { useRef, useState, useEffect, } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ScrollView, SafeAreaView, Alert, Image, TouchableOpacity } from 'react-native';

import { STRING_CONSTANTS, colors, fonts } from '../../utils';

import AppText from '../../components/text';
import { PrimaryButton,  } from '../../components/button';
import { CustomHeader } from '../../components/headers';
import { media } from '../../assets/media';
import LinearGradient from 'react-native-linear-gradient';

import Input from '../../components/input';
import { NAVIGATIONS } from '../../utils/constants/navigationConstants';
import CustomIcon from '../../components/CustomIcon';
import { password_validations } from '../../global/sampleData';
import { setAuthToken } from '../../store/modules/auth/actions';
import { useDispatch } from 'react-redux';

const CreatePassword = (props) => {

    const dispatch = useDispatch()
    const navigation = props?.navigation
    
    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});
    const [passwordValidations, setPasswordValidations] = useState(password_validations);

    const [showEyeIcon, setShowEyeIcon] = useState(false);
  

    const validatePassword = (password) => {
        const validations = [...passwordValidations];

        validations[0].isValid = password.length >= 8;
        validations[1].isValid = /\d/.test(password);
        validations[2].isValid = /[A-Z]/.test(password);
        
        setPasswordValidations(validations);
    };
    
    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
        validatePassword(value);
    }
  

    const areAllValidationsMet = () => {
        return passwordValidations.every(validation => validation.isValid);
    };

    const handleSubmit = () => {
        if (areAllValidationsMet()) {
            dispatch(setAuthToken('auth_token'))
            navigation.navigate(NAVIGATIONS.TABBAR)
        }
    };


    return (
        <>
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Create Password"} isBack />
            <ScrollView style={styles.mainContainer} >
                
                <View style={styles.imageContainer} >
                    <Image source={media.create_password} style={styles.loginImg} />
                </View>

                <AppText
                    text={'Choose a secure password that will be easy for you to remember.'}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{ fontSize: 18, lineHeight: 24, paddingHorizontal: 20, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                />

                <Input
                    placeholder="Enter your password"
                    isPassword
                    showEyeIcon={showEyeIcon}
                    error={errors.password}
                    inputIcon={media.password}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    value={form.password}
                    onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                />

                <View style={{marginVertical: 0}} >
                    {passwordValidations?.map((item, index) => (
                        <View key={index} style={styles.passwordValidations} >
                            <CustomIcon
                                iconName="check"
                                iconType="Feather"
                                iconSize={24}
                                iconColor={item.isValid ? colors.sea_green : '#A7AAB2'}
                            />
                            <AppText
                                text={item?.title}
                                type={STRING_CONSTANTS.textConstants.CAPTION1}
                                customStyle={{ color: item.isValid ? colors.sea_green : '#A6A3B8', fontSize: 18, lineHeight: 24, marginLeft: 10, fontFamily: fonts.primary_regular_font, fontWeight: '500', textAlign: 'center', }}
                            />
                        </View>
                    ))}
                    
                </View>


                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        handleSubmit()
                    }} 
                >
                    <LinearGradient
                        colors={['#8DF3ED', '#34D9D1']}
                        style={[styles.loginButton, {marginVertical: 20}]}
                    >
                        <AppText
                            text={'Continue'}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{color: colors.white, textAlign: 'center'}}
                        />
                    </LinearGradient>
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>

   
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        width: '100%', 
        padding: 20, 
        paddingHorizontal: 20,
        flex: 1,
    },
    imageContainer: {
        alignItems: 'center', 
        marginVertical: 20,
    },
    loginImg: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    passwordValidations: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8, 
    }
});


export default CreatePassword
