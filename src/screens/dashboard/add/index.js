import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, } from 'react-native'
import { colors } from '../../../utils'
import { CustomHeader } from '../../../components/headers'

const AddScreen = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >

            <CustomHeader title={"Add"} isBack />

            <View style={styles.mainContainer} >
                <Text style={{color: colors.white, fontSize: 20,}} >Add Screen</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default AddScreen