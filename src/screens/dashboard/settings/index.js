import React, { useState, useEffect, } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView, Switch, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import CustomIcon from '../../../components/CustomIcon'
import { media } from '../../../assets/media'
import AppText from '../../../components/text'
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'
import { profile_more_options, profile_options } from '../../../global/sampleData'
import { CustomHeader } from '../../../components/headers'
import { setAuthToken } from '../../../store/modules/auth/actions'
import { useDispatch } from 'react-redux'

const SettingsScreen = ({navigation}) => {

    const dispatch = useDispatch()
    const [userDetails, setUserDetails] = useState(null);

    const [profileOptions, setProfileOptions] = useState(profile_options);

    const [profileMoreOptions, setProfileMoreOptions] = useState(profile_more_options);

    useEffect(() => {
        setUserDetails({
            userName: 'Zaid Ahmed',
            profileType: 'Basic Member'
        })
    }, [])
    

    const profileFunctions = (item) => {
        if(item?.title == 'Sign Out'){
            dispatch(setAuthToken(null))
            navigation.navigate(NAVIGATIONS.LOGIN_STACK, {screen: NAVIGATIONS.GET_STARTED})
        }
        
    }

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Settings"} />
            <ScrollView>
                <View style={styles.mainContainer} >

                    <TouchableOpacity 
                        onPress={() => {navigation.navigate(NAVIGATIONS.PROFILE)}}
                        style={styles.profileContainer}>
                        <View style={styles.profileContent} >
                            <View style={styles.imageContainer} >
                                <Image source={media.user} style={{height: 24, width: 24, }} />
                            </View>

                            <View>
                                <AppText
                                    text={userDetails?.userName}
                                    type={STRING_CONSTANTS.textConstants.HEADING}
                                    customStyle={{color: colors.white  }}
                                />
                                <AppText
                                    text={userDetails?.profileType}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: colors.sea_green  }}
                                />
                            </View>
                        </View>
                        <View>
                            <CustomIcon
                                iconName="chevron-right"
                                iconType="Feather"
                                iconSize={30}
                                iconColor={colors.light_gray}
                            />
                        </View>
                    </TouchableOpacity>


                    <View style={{width: '100%', marginVertical: 20,}} >
                        <View style={styles.optionsHeading} >
                            <AppText
                                    text={'Accounts'}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                    customStyle={{color: colors.white  }}
                                />
                        </View>
                        <View style={{backgroundColor: colors.bg_color, paddingVertical: 10}} >
                            {profileOptions?.map((item, index) => (
                                <TouchableOpacity 
                                    activeOpacity={0.5}
                                    onPress={() => {profileFunctions(item)}}
                                    key={index} style={styles.optionContainer} >
                                    <View style={{alignItems: 'center', flexDirection: 'row', }} >
                                        <Image source={item?.icon} style={{height: 24, width: 24, resizeMode: 'containe', marginRight: 20 }} />
                                        <AppText
                                            text={item?.title}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                            customStyle={{color: colors.light_gray  }}
                                        />
                                    </View>
                                    <View>
                                        {item?.title != 'Sign Out'
                                        ?
                                            <CustomIcon
                                                iconName="chevron-right"
                                                iconType="Feather"
                                                iconSize={24}
                                                iconColor={colors.light_gray}
                                            />
                                        :
                                            null
                                        }
                                        
                                    </View>
                                    
                                </TouchableOpacity>
                            ))}
                        </View>
                        <View style={styles.moreOptionsHeading} >
                            <AppText
                                    text={'More Options'}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                    customStyle={{color: colors.sea_green  }}
                                />
                        </View>
                        <View style={{backgroundColor: colors.bg_color, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, paddingVertical: 10, marginBottom: 100}} >
                            {profileMoreOptions?.map((item, index) => (
                                <View key={index} style={styles.optionContainer} >
                                    <View style={{alignItems: 'center', flexDirection: 'row', }} >
                                        <Image source={item?.icon} style={{height: 24, width: 24, resizeMode: 'containe', marginRight: 20 }} />
                                        <AppText
                                            text={item?.title}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                            customStyle={{color: colors.light_gray  }}
                                        />
                                    </View>
                                    <View style={{}} >
                                        {item?.rightSide == 'switch'
                                        ?
                                            <Switch
                                                trackColor={{false: '#B4C2CD', true: '#81b0ff'}}
                                                thumbColor={false ? '#f5dd4b' : '#B4C2CD'}
                                                ios_backgroundColor="#1E2230"
                                                value={false}
                                                style={{borderWidth: 1, borderColor: colors.light_gray}}
                                            />
                                        :
                                            <View style={{ alignItems: 'center', flexDirection: 'row', }} >
                                                <View style={{width: 100, alignItems: 'flex-end'}} >
                                                    <AppText
                                                        text={item?.rightSide}
                                                        numberOfLines={1}
                                                        type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                                        customStyle={{color: colors.light_gray  }}
                                                    />
                                                </View>
                                                <CustomIcon
                                                    iconName="chevron-right"
                                                    iconType="Feather"
                                                    iconSize={24}
                                                    iconColor={colors.light_gray}
                                                />
                                            </View>
                                        }
                                        
                                    </View>
                                    
                                </View>
                            ))}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        paddingTop: 0,
    },
    profileContainer: {
        width: '100%',
        padding: 20,
        paddingVertical: 30,
        marginVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 14,
        justifyContent: 'space-between',
        backgroundColor: colors.bg_color,
    },
    profileContent: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    imageContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginRight: 14,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.light_gray
    },

    optionsHeading: {
        backgroundColor: colors.sea_green,
        justifyContent: 'center',
        paddingHorizontal: 20,
        height: 55,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    moreOptionsHeading: {
        backgroundColor: '#208DFE10',
        justifyContent: 'center',
        paddingHorizontal: 20,
        height: 55,
    },
    optionContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 15,
        paddingHorizontal: 20,
    },
    
})

export default SettingsScreen