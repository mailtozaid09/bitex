import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import AppText from '../../../components/text';
import { notification_data } from '../../../global/sampleData';
import { CustomHeader } from '../../../components/headers';

const NotificationScreen = ({navigation}) => {

    const [notifications, setNotifications] = useState(notification_data);

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Notification"} isBack />
            <ScrollView>
                <View style={styles.mainContainer} >
                    {notifications?.map((item, index) => (
                        <View key={index} style={styles.notificationContainer} >
                            <Image source={item?.icon} style={{height: 60, width: 60, resizeMode: 'contain', marginRight: 15,}} />

                            <View style={styles.textContent} >
                                <View style={styles.textheadingContent} >
                                    <AppText
                                        text={item?.title}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                        customStyle={{color: colors.light_gray  }}
                                    />
                                    <AppText
                                        text={item?.date}
                                        type={STRING_CONSTANTS.textConstants.BODY}
                                        customStyle={{color: colors.light_gray  }}
                                    />
                                </View>

                                <AppText
                                    text={item?.description}
                                    numberOfLines={2}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                    customStyle={{color: colors.light_gray, marginTop: 10, }}
                                />
                            </View>
                        </View>
                    ))}
                </View>
               
            </ScrollView>
            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
        marginBottom: 100,
        alignItems: 'center',
    },
    notificationContainer: {
        width: '100%',
        padding: 20,
        paddingVertical: 30,
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 14,
        backgroundColor: colors.bg_color,
    },
    textContent: {
        flex: 1,
    },
    textheadingContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
})

export default NotificationScreen