import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, } from 'react-native'
import { colors } from '../../../utils'

const CardScreen = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <Text>CardScreen</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default CardScreen