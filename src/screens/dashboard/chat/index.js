import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, } from 'react-native'
import { colors } from '../../../utils'

const ChatScreen = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <Text>ChatScreen</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default ChatScreen