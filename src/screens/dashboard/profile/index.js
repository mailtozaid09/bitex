import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { media } from '../../../assets/media'
import LinearGradient from 'react-native-linear-gradient'
import Input from '../../../components/input'
import AppText from '../../../components/text'
import { CustomHeader } from '../../../components/headers'

const ProfileScreen = (props) => {

    const navigation = props.navigation

    const [userDetails, setUserDetails] = useState(null);

    const [form, setForm] = useState({
        name: 'Zaid Ahmed',
        number: '9027346976',
        email: 'mailtozaid09@gmail.com',
        address: '9 New Road'
    })
    const [errors, setErrors] = useState({});


    useEffect(() => {
        setUserDetails({
            userName: 'Zaid Ahmed',
            profileType: 'Basic Member',
            total_amount: '200.00'
        })
    }, [])

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Profile Details"} isBack />
            <ScrollView>
            <View style={styles.mainContainer} >
                <View style={styles.profileContent} >
                    <View style={styles.imageContainer} >
                        <Image source={media.user} style={{height: 40, width: 40,  }} />
                    </View>

                    <View style={{alignItems: 'center', marginVertical: 20}} >
                        <AppText
                            text={userDetails?.userName}
                            type={STRING_CONSTANTS.textConstants.HEADING}
                            customStyle={{color: colors.white,   }}
                        />
                        <View style={{alignItems: 'center', flexDirection: 'row', marginTop: 10,}} >
                            <AppText
                                text={'Account Credit: '}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.sea_green,  }}
                            />
                            <AppText
                                text={`$${userDetails?.total_amount}`}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: '#FE8270',  }}
                            />
                        </View>
                    </View>
                </View>

             
                <View style={{width: '100%', flex: 1, paddingHorizontal: 25, marginVertical: 25}} > 


                    <Input
                        placeholder="Enter your name"
                        value={form.name}
                        error={errors.name}
                        isEditable
                        onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                    />

                    <Input
                        placeholder="Enter your number"
                        value={form.number}
                        error={errors.number}
                        isPhoneNumber
                        isEditable
                        onChangeText={(text) => {onChange({name: 'number', value: text,}); setErrors({}); }}
                    />


                    <Input
                        placeholder="Enter your email"
                        value={form.email}
                        error={errors.email}
                        isEditable
                        onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                    />

                    <Input
                        placeholder="Enter your address"
                        value={form.address}
                        error={errors.address}
                        isEditable
                        onChangeText={(text) => {onChange({name: 'address', value: text,}); setErrors({}); }}
                    />



                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                            navigation.goBack()
                        }} 
                    >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.loginButton, {marginVertical: 20}]}
                        >
                            <AppText
                                text={'Save Now'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View> 
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
    },
    profileContent: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageContainer: {
        height: 120,
        width: 120,
        borderRadius: 60,
        marginRight: 14,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginVertical: 10,
        borderColor: colors.light_gray
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },


})

export default ProfileScreen