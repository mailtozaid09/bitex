import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, ScrollView, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { bitcoin_data, } from '../../../global/sampleData';
import AppText from '../../../components/text';
import Input from '../../../components/input';
import { media } from '../../../assets/media';
import LinearGradient from 'react-native-linear-gradient';
import CustomIcon from '../../../components/CustomIcon';
import { CustomHeader } from '../../../components/headers';
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants';

const PortfolioScreen = ({navigation}) => {

    const [searchText, setSearchText] = useState('');
    const [data, setData] = useState(bitcoin_data);


    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader 
                title={"Portfolio"}
                isBack 
                isRight={(
                    <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.black,}} >
                        <CustomIcon
                            iconName="dots-three-vertical"
                            iconType="Entypo"
                            iconSize={20}
                            iconColor={colors.white}
                        />
                    </View>
                )} 
            />
            

            <ScrollView>
                <View style={styles.mainContainer} >

                    <View style={{marginVertical: 20,}} >
                        <Image source={media.portfolio_bg} style={styles.background} />

                        <View style={{position: 'absolute', width: '100%', justifyContent: 'space-between',  height: 200, padding: 20, paddingVertical: 25 }} >

                            <LinearGradient
                                colors={['#C1B2FF', '#9B87FF']}
                                style={styles.btcContainer}
                            >
                                <AppText
                                    text={'BTC/USD'}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: colors.white, textAlign: 'center'}}
                                />
                                <CustomIcon
                                    iconName="chevron-down"
                                    iconType="Feather"
                                    iconSize={24}
                                    iconColor={colors.white}
                                />
                            </LinearGradient>

                            <AppText
                                text={'$ 5,781.00'}
                                type={STRING_CONSTANTS.textConstants.HEADING}
                                customStyle={{color: colors.white, marginVertical: 10, fontSize: 30, fontWeight: 'bold' }}
                            />
                        
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                <AppText
                                    text={'+ $248.23 (+0.35)'}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                    customStyle={{color: colors.white, textAlign: 'center'}}
                                />

                                <LinearGradient
                                    colors={['#FF9BD0', '#FF73AA']}
                                    style={styles.btcContainer}
                                >
                                    <AppText
                                        text={'Add Balance'}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                        customStyle={{color: colors.white, textAlign: 'center'}}
                                    />
                                </LinearGradient>
                            </View>
                                    
                            
                        </View>
                    </View>


                    <View style={styles.buttonContainer} >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.buttons, {marginRight: 10}]}
                        >
                            <AppText
                                text={'Highest holdings'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                        <LinearGradient
                            colors={['#C1B2FF', '#9B87FF']}
                            style={[styles.buttons, {marginLeft: 10}]}
                        >
                            <AppText
                                text={'24 Hours'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </View>
               
                    <FlatList
                        data={data}
                        keyExtractor={item => item.id}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({item, index}) => (
                            <TouchableOpacity 
                                activeOpacity={0.5}
                                onPress={() => {navigation.navigate(NAVIGATIONS.PORTFOLIO_DETAILS, {params: item})}}
                                key={index} style={[styles.cardContainer, {backgroundColor: colors.bg_color}]} >

                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                        <Image source={item?.icon} style={{height: 60, width: 60, marginRight: 15, resizeMode: 'contain'}} />
                                        <View>
                                            <AppText
                                                text={item?.title}
                                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                                customStyle={{color: colors.white, }}
                                            />
                                            <AppText
                                                text={item?.subTitle}
                                                type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                                customStyle={{color: '#A6A3B8', marginTop: 10 }}
                                            />
                                        </View>
                                    </View>
                                    <View style={{alignItems: 'flex-end'}} >
                                        <AppText
                                            text={`$${item?.price}`}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                            customStyle={{color: colors.white, fontWeight: 'bold' }}
                                        />
                                        <View style={{alignItems: 'center', justifyContent: 'center',}} >
                                            <AppText
                                                text={`${item?.percentage}%`}
                                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                                customStyle={{color: item?.bg_color  }}
                                            />
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        padding: 20,
        paddingTop: 0
    },
    cardContainer: {
        padding: 20,
        borderRadius: 15,
        marginBottom: 20,
        width: '100%',
    },
    background: {
        height: 200,
        width: '100%',
        borderRadius: 15,
    },
    btcContainer: {
        height: 40,
        width: 140,
        borderRadius: 20,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    buttons: {
        height: 50,
        flex: 1,
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    buttonContainer: {
        marginVertical: 30, 
        marginBottom: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },

    
})

export default PortfolioScreen