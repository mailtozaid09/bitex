import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, ScrollView, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { bitcoin_data, bitcoin_sub_data, } from '../../../global/sampleData';
import AppText from '../../../components/text';
import Input from '../../../components/input';
import { media } from '../../../assets/media';
import LinearGradient from 'react-native-linear-gradient';
import CustomIcon from '../../../components/CustomIcon';
import { CustomHeader } from '../../../components/headers';

const PortfolioDetails = (props) => {

    const [searchText, setSearchText] = useState('');
    const [data, setData] = useState(bitcoin_data);

    const [details, setDetails] = useState({});

    useEffect(() => {
        let details = props?.route?.params?.params
        setDetails(details)
    }, [])
    

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader 
                title={`${details?.title} (${details?.subTitle})`}
                isBack 
                isRight={(
                    <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.black,}} >
                        <CustomIcon
                            iconName="hearto"
                            iconType="AntDesign"
                            iconSize={20}
                            iconColor={colors.white}
                        />
                    </View>
                )} 
            />
            

            <ScrollView>
                <View style={styles.mainContainer} >


                <View style={styles.cardContainer} >

                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Image source={details?.icon} style={{height: 60, width: 60, marginRight: 15, resizeMode: 'contain'}} />
                            <View>
                                <AppText
                                    text={details?.title}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: colors.white, }}
                                />
                                <AppText
                                    text={details?.subTitle}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                    customStyle={{color: '#A6A3B8', marginTop: 10 }}
                                />
                            </View>
                        </View>
                        <View style={{alignItems: 'flex-end'}} >
                            <AppText
                                text={`$${details?.price}`}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, fontWeight: 'bold' }}
                            />
                            <View style={{alignItems: 'center', justifyContent: 'center',}} >
                                <AppText
                                    text={`${details?.percentage}%`}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: details?.bg_color  }}
                                />
                            </View>
                        </View>
                    </View>
                    </View>


                    <View style={styles.buttonContainer} >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.buttons, {marginRight: 10, flex: 0, width: 170}]}
                        >
                            <AppText
                                text={'Global Average'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                        <View style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Image source={media.candle_icon} style={{height: 50, width: 50, resizeMode: 'contain', marginRight: 15}} />
                            <Image source={media.expand} style={{height: 50, width: 50, resizeMode: 'contain', marginRight: 15}} />
                        </View>
                    </View>

                    <Image source={media.graph} style={{height: 400, width: '100%', resizeMode: 'contain', marginRight: 15}} />
               
                    <FlatList
                        data={bitcoin_sub_data}
                        keyExtractor={item => item.id}
                        renderItem={({item, index}) => (
                            <View key={index} style={styles.cardContainer} >
                                <AppText
                                    text={item?.title}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{color: colors.sea_green,}}
                                />
                                <AppText
                                    text={item?.value}
                                    type={STRING_CONSTANTS.textConstants.HEADING}
                                    customStyle={{color: colors.white, fontWeight: 'bold', marginTop: 10}}
                                />
                            </View>
                        )}
                    />


                    <View style={styles.buttonContainer} >
                        <LinearGradient
                            colors={['#C1B2FF', '#9B87FF']}
                            style={[styles.buttons, {marginRight: 10}]}
                        >
                            <AppText
                                text={'Sell'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.buttons, {marginRight: 10}]}
                        >
                            <AppText
                                text={'Buy'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        padding: 20,
        paddingTop: 10
    },
    cardContainer: {
        padding: 20,
        borderRadius: 15,
        marginBottom: 20,
        width: '100%',
        backgroundColor: colors.bg_color,
    },
    buttons: {
        height: 50,
        flex: 1,
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    buttonContainer: {
        marginVertical: 10, 
        marginBottom: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    
})

export default PortfolioDetails