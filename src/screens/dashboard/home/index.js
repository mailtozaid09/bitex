import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView, BackHandler, StatusBar, } from 'react-native'
import { colors } from '../../../utils'
import HomeHeader from '../../../components/headers/HomeHeader'
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants'
import HomeBanner from '../../../components/banner/HomeBanner'
import PortfolioCard from '../../../components/custom/home/PortfolioCard'
import MarketCard from '../../../components/custom/home/MarketCard'
import HomeOptions from '../../../components/custom/home/HomeOptions'

const HomeScreen = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >

            <ScrollView>
                <HomeHeader title="Hi Zaid Ahmed" onMenuPress={() => {}} onBellPress={() => {navigation.navigate(NAVIGATIONS.NOTIFICATION)}} />
                <View style={styles.mainContainer} >
                    <HomeBanner />

                    <PortfolioCard onPress={() => {navigation.navigate(NAVIGATIONS.PORTFOLIO)}} />

                    <HomeOptions />

                    <MarketCard onPress={() => {navigation.navigate(NAVIGATIONS.MARKET)}} />
                </View>
            </ScrollView>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.black,
    },
    mainContainer: {
        flex: 1,
        marginTop: 20,
        marginBottom: 30,
        padding: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignItems: 'center',
        backgroundColor: colors.primary
    }
})

export default HomeScreen