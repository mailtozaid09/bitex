import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, ScrollView, } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { bitcoin_data,  } from '../../../global/sampleData';
import AppText from '../../../components/text';
import Input from '../../../components/input';
import { CustomHeader } from '../../../components/headers';
import CustomIcon from '../../../components/CustomIcon';
import { NAVIGATIONS } from '../../../utils/constants/navigationConstants';

const MarketTrends = ({navigation}) => {

    const [searchText, setSearchText] = useState('');
    const [data, setData] = useState(bitcoin_data);


    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader 
                title={"Market Trends"} 
                isBack 
                isRight={(
                    <View style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.black,}} >
                        <CustomIcon
                            iconName="search1"
                            iconType="AntDesign"
                            iconSize={20}
                            iconColor={colors.sea_green}
                        />
                    </View>
                )} 
            />
            <ScrollView>
                <View style={styles.mainContainer} >
                
                <Input
                    isSearch
                    placeholder="Search Currency . . ."
                    value={searchText}
                    onChangeText={(text) => { setSearchText(text) }}
                />

                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({item, index}) => (
                        <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate(NAVIGATIONS.PORTFOLIO_DETAILS, {params: item})}}
                        key={index} style={[styles.cardContainer, {backgroundColor: colors.bg_color}]} >

                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}>
                                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                    <Image source={item?.icon} style={{height: 60, width: 60, marginRight: 15, resizeMode: 'contain'}} />
                                    <View>
                                        <AppText
                                            text={item?.title}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                            customStyle={{color: colors.white, }}
                                        />
                                        <AppText
                                            text={item?.subTitle}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE3}
                                            customStyle={{color: '#A6A3B8', marginTop: 10 }}
                                        />
                                    </View>
                                </View>
                                <View style={{alignItems: 'flex-end'}} >
                                    <AppText
                                        text={`$${item?.price}`}
                                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                        customStyle={{color: colors.white, fontWeight: 'bold' }}
                                    />
                                    <View style={{alignItems: 'center', justifyContent: 'center',}} >
                                        <AppText
                                            text={`${item?.percentage}%`}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                            customStyle={{color: item?.bg_color  }}
                                        />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        padding: 20,
        paddingTop: 10,
        paddingBottom: 100,
    },
    cardContainer: {
        padding: 20,
        borderRadius: 15,
        marginBottom: 20,
        width: '100%',
    },
})

export default MarketTrends