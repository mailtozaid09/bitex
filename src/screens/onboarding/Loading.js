import React, { useEffect, useState } from 'react'
import { SafeAreaView, Text, View, StyleSheet, Image, } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import AppText from '../../components/text'
import { media } from '../../assets/media'
import { NAVIGATIONS } from '../../utils/constants/navigationConstants'
import { useNavigation } from '@react-navigation/native';
import * as Progress from 'react-native-progress';

const LoadingScreen = ({ navigation }) => {

    const [progress, setProgress] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setProgress((prevProgress) => {
                if (prevProgress >= 1) {
                    clearInterval(interval);
                    navigation.navigate('Onboarding'); 
                    return 1;
                }
                return prevProgress + 0.25;
            });
        }, 1000); // 1000 ms = 1 second

        return () => clearInterval(interval);
    }, [navigation]);
    

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >

                <Image
                    source={media.app_icon}
                    style={styles.app_icon}
                />
                <AppText
                    text={"BITEX"}
                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                    customStyle={{ fontSize: 50, color: colors.white, }}
                />

                <Progress.Bar 
                    progress={progress} 
                    width={300} 
                    height={10}
                    borderRadius={10}
                    color={colors.sea_green}
                    unfilledColor={colors.white}
                    style={{marginVertical: 20}} 
                />
                <AppText
                    text={'Loading....'}
                    numberOfLines={1}
                    type={STRING_CONSTANTS.textConstants.HEADING}
                    customStyle={{color: '#9C88FF' }}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    app_icon: {
        height: 200,
        width: 200,
        margin: 20
    }
})

export default LoadingScreen