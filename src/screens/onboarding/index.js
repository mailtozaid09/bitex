import React, { useState, useEffect } from 'react'
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
 
import { onboarding_data } from '../../global/sampleData'
import AppText from '../../components/text'

import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useDispatch } from 'react-redux'
import { setOnboardingDetails } from '../../store/modules/auth/actions'

const OnboardingScreen = ({navigation}) => {

    const dispatch = useDispatch()
    
    const [onboardingDetails, setOnboardDetails] = useState(onboarding_data);
    const [currentIdx, setCurrentIdx] = useState(0);

    const nextButton = () => {
        if(currentIdx == onboardingDetails?.length - 1){
            completeOnboarding()
        }else{
            setCurrentIdx(prev => prev + 1)
        }
    }

    const completeOnboarding = () => {
        AsyncStorage.setItem("onboarding_done", "true");
        dispatch(setOnboardingDetails(true))
        navigation.navigate('GetStarted');
    }

    return (
        <SafeAreaView style={styles.container} >

            <View style={styles.mainContainer} >

                <LinearGradient
                    colors={['#FF9BD0', '#FF73AA']}
                    style={styles.stepContainer}
                >
                    <AppText
                        text={`${currentIdx+1} of 3`}
                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                        customStyle={{color: colors.white, textAlign: 'center'}}
                    />
                </LinearGradient>


                <View style={{flex: 1,  }} >
                    <Image
                        source={onboardingDetails[currentIdx]?.image}
                        style={styles.onboardingImg}
                    />
                </View>


                <View style={{marginVertical: 20, alignItems: 'center', justifyContent: 'center', width: '100%', }} >
                    <AppText
                        text={onboardingDetails[currentIdx]?.title}
                        type={STRING_CONSTANTS.textConstants.HEADING}
                        customStyle={{ fontSize: 30, lineHeight: 34, fontFamily: fonts.primary_bold_font, fontWeight: 'bold', color: colors.sea_green, textAlign: 'center'}}
                    />
                    <AppText
                        text={onboardingDetails[currentIdx]?.description}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                    />
                    <TouchableOpacity
                            activeOpacity={0.5}
                            style={styles.getStarted}
                            onPress={() => {
                                completeOnboarding()
                            }}
                        >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={styles.getStarted}
                        >
                            <AppText
                                text={`Get Started`}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{alignItems: 'center', marginBottom: 10 }} >
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => { nextButton() }}
                >
                <LinearGradient
                    colors={['#FF9BD0', '#FF73AA']}
                    style={[styles.stepContainer, {height: 44, width: 120, borderRadius: 22, marginBottom: 10}]}
                >
                    <AppText
                        text={'Next'}
                        type={STRING_CONSTANTS.textConstants.HEADLINE2}
                        customStyle={{color: colors.white, textAlign: 'center'}}
                    />
                </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        completeOnboarding()
                    }}
                >
                    <AppText
                        text={`Skip`}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{color: colors.white, textDecorationLine: 'underline', textAlign: 'center'}}
                    />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1, 
        margin: 20, 
        padding: 20,
        paddingTop: 40,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.black,
    },
    stepContainer: {
        height: 35,
        width: 85,
        borderRadius: 20,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    getStarted: {
        height: 50,
        width: '80%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 20,
    },
    onboardingImg: {
        flex: 1,
        height: 200,
        width: 200,
        marginVertical: 0,
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
   
export default OnboardingScreen