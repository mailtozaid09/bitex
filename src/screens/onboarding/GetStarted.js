import React, { useState, useEffect } from 'react'
import { Text, View, Image, StyleSheet, SafeAreaView, TouchableOpacity, BackHandler, } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import AppText from '../../components/text'
import AlreadyHaveAccount from '../../components/custom/login/AlreadyHaveAccount'
import { PrimaryButton } from '../../components/button'
import { media } from '../../assets/media'
import { screenWidth } from '../../utils/constants'
import LinearGradient from 'react-native-linear-gradient'
import { NAVIGATIONS } from '../../utils/constants/navigationConstants'
 

const GetStarted = ({navigation}) => {

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}>
                <View style={styles.imageContainer} >
                    <Image
                        source={media.onboarding_1}
                        style={styles.getstartedImg}
                    />
                </View>

                <View style={{paddingHorizontal: 30, marginVertical: 20}} >
                    <AppText
                        text={'Welcome to Cryptocurrency'}
                        type={STRING_CONSTANTS.textConstants.HEADING}
                        customStyle={{ fontSize: 30, lineHeight: 34, fontFamily: fonts.primary_bold_font, fontWeight: 'bold', color: colors.sea_green, textAlign: 'center'}}
                    />
                    <AppText
                        text={'Deliver your Order around the world without hesitation'}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{ fontSize: 18, lineHeight: 24, fontFamily: fonts.primary_regular_font, fontWeight: '500', color: '#A6A3B8', textAlign: 'center', marginVertical: 20}}
                    />
                </View>


                <View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate(NAVIGATIONS.LOGIN)}}
                    >
                        <LinearGradient
                            colors={['#C1B2FF', '#9B87FF']}
                            style={styles.loginButton}
                        >
                            <AppText
                                text={'Login'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate(NAVIGATIONS.SIGN_UP)}}
                    >
                        <LinearGradient
                            colors={['#8DF3ED', '#34D9D1']}
                            style={[styles.loginButton, {marginTop: 20}]}
                        >
                            <AppText
                                text={'Register'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                customStyle={{color: colors.white, textAlign: 'center'}}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>

            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
    },
    mainContainer: {
        flex: 1, 
        padding: 20,
        width: '100%',
        justifyContent: 'space-between',
    },
    imageContainer: {
        width: '100%',
        height: screenWidth-50,
        paddingVertical: 50,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black,
    },
    getstartedImg: {
        height: 200,
        width: 200,
    },
    loginButton: {
        height: 50,
        width: '100%',
        borderRadius: 25,
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

})

export default GetStarted