import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image, StyleSheet } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { Navigator } from './src/navigator';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/store';

import SplashScreen from 'react-native-splash-screen';
import { AuthProvider } from './src/context/AuthContext';
import { ToastProvider } from './src/context/ToastContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { colors } from './src/utils';

LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
        //AsyncStorage.clear()
    }, [])
    
    return (
        
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <AuthProvider>
                    <ToastProvider>

                    <View style={styles.container}>
                        <StatusBar
                            animated={true}
                            backgroundColor={colors.primary}
                            barStyle="light-content"
                            showHideTransition="fade"
                        />
                        <NavigationContainer>
                            <Navigator />
                        </NavigationContainer>
                    </View>
                    </ToastProvider>
                </AuthProvider>
            </PersistGate>
        </Provider>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
        paddingTop: Platform.OS === 'ios' ? 60 : 0,
    },
});


export default App
